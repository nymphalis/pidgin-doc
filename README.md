## Anleitung für Pidgin zum IRC Chatten

Der SMJG Chat basiert auf dem sogenannten "IRC" Protokoll, wofür es verschiedene Clients gibt. Ich zeige hier die Einrichtung für
den populären Client "Pidgin".

Zuerst musst du dir den Pidgin Client herunterladen: [link](https://pidgin.im/download/). Wenn du Linux verwendest, bietet
das Betriebssystem meistens eigene Mittel an, Software zu installieren. Zum Beispiel bei Ubuntu gibt es dazu z.B. das Programm "Ubuntu Software".


Das ist das Program mit dem du den Smjg-Chat betreten kannst. Nach dem Start siehst du ein ähnliches Bild, wie dieses: 

![](1.png)

Nach einem Klick auf "Hinzufügen" öffnet sich ein neues Fenster, in welchem du nun zuerst bei "Protokoll" "IRC" auswählen musst. 

![](2.png)

Im Feld "Benutzer" gibst du dann deinen Nickname ein (der Name, unter dem du im Chat angesprochen/angezeigt werden willst). 
Desweiteren musst du im Feld "Server" "irc.fu-berlin.de" (ohne die Anführungszeichen) eintragen. Das Ergebnis sollte dann in etwa so aussehen:

![](3.png)


Danach musst du auf den Reiter "Erweitert" klicken:

![](4.png)

Hier trägst du im Feld "Benutzername" und "Echter Name" am Besten den gleichen Nick ein, wie du ihn schon früher im Feld "Benutzer" eingegeben hast.
(Natürlich kann man im Feld "Echter Name" auch den echten Namen eingeben, aber an der Funktionalität ändert das nichts.)
Zusätzlich solltest du noch ein Häckchen bei "Eingehendes UTF-8 automatisch erkennen" setzen.

Das sollte dann so aussehen:

![](5.png)

Danach auf "Hinzufügen" klicken. Nun solltest du folgendes sehen können:

![](6.png)


Nachfolgend im linken Fenster ganz links oben auf "Kontakte" klicken und dort "Chat hinzufügen" auswählen.

![](7.png)


Dann sollte dieses neue Fenster erscheinen:

![](8.png)

Dort bei "Kanal" "!smjg" eingeben. Wenn man möchte, kann man noch zum Beispiel einen Hacken setzen bei "Automatisch beitreten, wenn sich
das Konto verbindet". Das Ergebnis sollte dann so aussehen:

![](9.png)

Nach dem Klick auf "Hinzufügen" sollte sich das Hauptfenster von Pidgin etwas verändern, etwa so:

![](10.png)

Mit einem Doppelklick auf "!smjg" unterhalb von "Chats" könnt ihr nun den SMJG Channel betreten.

Viel Spaß :)

